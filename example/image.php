<style>
#wrapper { width: 600px; margin: 0 auto }
#map_canvas { height: 500px; width: 600px }
</style>
<div id="wrapper">
    <div id="map_canvas"></div>
</div>
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script>
function initialize() {
    var stylez = [
      {
        featureType: "all",
        stylers: [
          { hue: "#0000ff" },
          { saturation: -75 }
        ]
      },
      {
        featureType: "poi",
        elementType: "label",
        stylers: [
          { visibility: "off" }
        ]
      }
    ];
    
    var locations = [
      ['Bondi Beach', 10.0843179, 77.9610158, 4],
      ['Coogee Beach', 10.133035, 77.924223, 5],
      ['Cronulla Beach', 10.103199, 77.946947, 3],
      ['Manly Beach', 9.9252007, 78.1197753, 2],
      ['Maroubra Beach', 11.0168445, 76.95583209, 1]
    ];
    
    var pointA = new google.maps.LatLng(11.0168445, 76.95583209),
   var  pointB = new google.maps.LatLng(10.0843179, 76.95583209),

    var latlng = new google.maps.LatLng(11.652236, 78.793945), // Stockholm
    
        mapOptions = {
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.ROADMAP, "Edited"] 
            },
            zoom: 14,
            center: latlng
        },
        
        
    
    	directionsService = new google.maps.DirectionsService,
    
    
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions),
        
        styledMapType = new google.maps.StyledMapType(stylez, {name: "Edited"}),
        
         var marker, i;

	    for (i = 0; i < locations.length; i++) {
	      marker = new google.maps.Marker({
		position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		map: map
	    }),
      
       /* marker = new google.maps.Marker({
            position: latlng, 
            map: map, 
            animation: google.maps.Animation.DROP,
            title:"Hello World!"
        }),*/
        
        infowindow = new google.maps.InfoWindow({
            content: "<div><img width='254' height='355' src='http://www.hyperpac.de/wp-content/uploads/2009/09/255px-Excitebike_cover.jpg'</div>"
        });
        }
        
        directionsService.route({
	    origin: pointA,
	    destination: pointB,
	    travelMode: google.maps.TravelMode.DRIVING
	  }, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	      directionsDisplay.setDirections(response);
	    } else {
	      window.alert('Directions request failed due to ' + status);
	    }
	  });
	  
        map.mapTypes.set("Edited", styledMapType);
        map.setMapTypeId('Edited');
    
    function toggleBounce () {
        if (marker.getAnimation() != null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }
    
    // Add click listener to toggle bounce
    google.maps.event.addListener(marker, 'click', function () {
        toggleBounce();
        infowindow.open(map, marker);
        setTimeout(toggleBounce, 1500);
    });
}

// Call initialize -- in prod, add this to window.onload or some other DOM ready alternative
initialize();
</script>

