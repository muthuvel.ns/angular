var helloApp = angular.module("helloApp", []);
helloApp.config(['$httpProvider', function($httpProvider) {
       $httpProvider.defaults.useXDomain = true;
    	delete $httpProvider.defaults.headers.common['X-Requested-With'];
    	$httpProvider.defaults.headers.common["Accept"] = "application/json";
    	$httpProvider.defaults.headers.common["Content-Type"] = "application/json";
	delete $httpProvider.defaults.headers.post['Content-type'];
    }
]);


helloApp.controller("CompanyCtrl", function($scope,$http) {

//var baseurl = 'http://128.199.101.112/securitysystem/index.php/v1/getuserlist';
//http://localhost:8084/load
var baseurl = 'http://localhost:8084/v1/marketplace';
var req =  {"marketplace":{"city":"All","type":"1","subtype":"6","startindex":"0","noofcount":"20"}};

//var req =  {"marketplace":{"mcity":"All","maddress":"All","mtype":"1","startindex":"0","noofcount":"5"}};


$http({
    method: 'POST',
    url: baseurl,
    data: req,
    headers: {'Content-Type': 'application/json','action':'select','GuestUser':'1'}
}).then(function(response) { console.log(response.data.data);
/*		var input = response.data.data.dataList;
	angular.forEach(input, function(value, key){
     		console.log(key + ': ' + value);
     		$scope.companies=value;
	});*/
	$scope.companies=response.data.data.dataList;
	
	
    if (response.data == 'ok') {
        // success
    } else {
        // failed
    }
});

/*
$http.get('http://localhost:8069/select').success(function(data){console.log(data);
$scope.companies=data;

});*/


$scope.addRow = function(){		
	//$scope.companies.push({ 'name':$scope.name, 'employees': $scope.employees, 'headoffice':$scope.headoffice });
	//$scope.name='';
	//$scope.employees='';
	//$scope.headoffice='';
	//console.log($scope.companies);
	
	$http({
  method: 'POST',
  url: 'http://localhost:8069/insert',
headers: {
   'Content-Type':'application/json'
 },
 data: { 'name':$scope.name, 'employees': $scope.employees, 'headoffice':$scope.headoffice }

}).then(function successCallback(response) {
	$scope.companies.push({ 'name':$scope.name, 'employees': $scope.employees, 'headoffice':$scope.headoffice });
	$scope.name='';
	$scope.employees='';
	$scope.headoffice='';
  }, function errorCallback(response) {
  });
  
};

$scope.removeRow = function(name){


$http({
  method: 'POST',
  url: 'http://localhost:8069/delete',
headers: {
   'Content-Type':'application/json'
 },
 data: { name: name }

}).then(function successCallback(response) {console.log(response);
    // this callback will be called asynchronously
    // when the response is available
//$scope.companies=response.data;

		var index = -1;		
		var comArr = eval( $scope.companies );
		for( var i = 0; i < comArr.length; i++ ) {
			if( comArr[i].name === name ) {
				index = i;
				break;
			}
		}
		if( index === -1 ) {
			alert( "Something gone wrong" );
		}
		$scope.companies.splice( index, 1 );	
  }, function errorCallback(response) {
    // called asynchronously if an error occurs
    // or server returns response with an error status.
  });			
			
	};
});
