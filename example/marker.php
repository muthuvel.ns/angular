<html>
<head>
  
  <title>Google Maps Multiple Markers</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
</head>
<body>
		
  <div id="map" style="height: 800px; width: 1300;">
</div>
<script type="text/javascript">
     var locations = [
      ['vadipatti', 10.0843179, 77.9610158, "https://61d6aef7250419f32a21-fccc65eb10c2e292cbf7c8403d22d108.ssl.cf1.rackcdn.com/63ef3853-8b68-47a7-80e9-ae56dc503c88.jpg",4],
      ['Palladam', 10.991548, 77.282639, "https://www.seoclerk.com/pics/trade2684-1KYOoJ1416509800.jpg",5],
      ['oddanchatram', 10.478832, 77.761917, "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSF7D-Sfk7FjQfNE3ayFvvavqij6lATaW-B554op02VkObq6Kov",3],
      ['Dharapuram', 10.727203, 77.524338, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfMwp8biITBZ-jlnRzBhXgCsWXBrHjyyqdgfB6OW3UyQ3AXWxf",2],
      ['chinnalapatti', 10.278910, 77.932205, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwF19ICwtlFY9hqLjohfZrF6UiS5mPRn2nYKJgymIZP6mcB8ahfg",2],
      ['Coimbatore', 11.0168445, 76.95583209, "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSfMwp8biITBZ-jlnRzBhXgCsWXBrHjyyqdgfB6OW3UyQ3AXWxf",1]
    ];
    
     var pointA = new google.maps.LatLng(11.0168445, 76.95583209),
   	pointB = new google.maps.LatLng(10.0843179, 77.9610158),
   directionsService = new google.maps.DirectionsService;

 
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(11.0168445, 76.95583209),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

   directionsDisplay = new google.maps.DirectionsRenderer({
      map: map
    });
    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent('<iframe class="cycle" src="mapinfo.htm" height="300px" width="500px"></iframe>');
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
   /*  directionsService.route({
	    origin: pointA,
	    destination: pointB,
	    travelMode: google.maps.TravelMode.DRIVING
	  }, function(response, status) {
	    if (status == google.maps.DirectionsStatus.OK) {
	      directionsDisplay.setDirections(response);
	    } else {
	      window.alert('Directions request failed due to ' + status);
	    }
	  });*/
	  
	  var cityCircle = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: {lat: 11.0168445, lng: 76.95583209}, //search location lat and lng
            radius: 50000
          });
  </script>
  
  </body>
</html>
