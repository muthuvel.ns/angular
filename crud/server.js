var express = require("/node_modules/express");
var mysql = require("/node_modules/mysql");
var bodyParser = require('/node_modules/body-parser');
var fileUpload = require('/node_modules/fileupload');
var app = express();


// configure the app to use bodyParser()
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});

/*
* Configure MySQL parameters.
*/
var connection = mysql.createConnection({
host : "localhost",
user : "root",
password : "123",
database : "test"
});

/*Connecting to Database*/

connection.connect(function(error){
if(error)
{
console.log("Problem with MySQL"+error);
}
else
{
console.log("Connected with Database");
}
});

/*Start the Server*/
var port = 8069;
app.listen(port,function(){
console.log("It's Started on PORT "+port);
});

app.get('/',function(req,res){

res.sendfile('index.html');
});


/*
* Here we will call Database.
* Fetch news from table.
* Return it in JSON.
*/
app.post('/select',function(req,res){
var id = req.body.id; // get body parser based. need to add header in postman Content-Type: application/json
console.log(req.body.id);
var where = "SELECT id,name,employees,headoffice from company WHERE id IN (?)";
if(req.body.id =='' || req.body.id == undefined){
where = "SELECT id,name,employees,headoffice from company";
}
console.log(where);
connection.query(where,id,function(err,rows){
if(err)
{
console.log("Problem with MySQL"+err);
}
else
{
res.end(JSON.stringify(rows));
}
});

});

app.post('/delete',function(req,res){
var id = req.body.id; // get body parser based. need to add header in postman Content-Type: application/json
console.log(id);
var where = "Delete from company WHERE id IN (?)";
connection.query(where,id,function(err,rows){
if(err)
{
console.log("Problem with MySQL"+err);
}
else
{
res.end(JSON.stringify(rows));
}
});
})

app.post('/insert',function(req,res){
 // get body parser based. need to add header in postman Content-Type: application/json

var where = "INSERT INTO company SET ?";
var data  = {
  name: req.body.name,
  employees: req.body.employees,
  headoffice: req.body.headoffice,
};

console.log(data);

connection.query(where,data,function(err,rows){
if(err)
{
console.log("Problem with MySQL"+err);
}
else
{
res.end(JSON.stringify(rows));
}
});

});

app.post('/update',function(req,res){
 // get body parser based. need to add header in postman Content-Type: application/json
var id = req.body.id;
var data  = {
  name: req.body.name,
  employees: req.body.employees,
  headoffice: req.body.headoffice,
};

console.log(data);

connection.query('UPDATE company SET ? WHERE id = ?', [data, id],function(err,rows){
if(err)
{
console.log("Problem with MySQL"+err);
}
else
{
res.end(JSON.stringify(rows));
}
});

});


 
/*app.post('/upload', function(req, res) {
    var sampleFile;
 
    if (!req.files) {
        res.send('No files were uploaded.');
        return;
    }
 
    sampleFile = req.files.sampleFile;
    sampleFile.mv('/somewhere/on/your/server/filename.jpg', function(err) {
        if (err) {
            res.status(500).send(err);
        }
        else {
            res.send('File uploaded!');
        }
    });
});*/

